#!/bin/bash
set +x

ARCHITECTURE=$(dpkg --print-architecture)

# install docker
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=$ARCHITECTURE] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
sudo apt install docker-ce -y

if [[ $ARCHITECTURE == 'amd64' ]]
then
    # next two lines of code has not been tested - Past Robert
    TAG=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep "html_url" | grep releases | awk -F '/' '{print $NF}' | tr -d '",')
    sudo curl -L "https://github.com/docker/compose/releases/download/$TAG/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
else
    sudo apt install python3-pip
    sudo pip3 install docker-compose
fi

docker --version
docker-compose --version

# give out docker sudo permissions to user
sudo usermod -aG docker ${USER}