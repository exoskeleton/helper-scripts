#!/bin/zsh

for compose in $(ls */docker-compose.yml) 
do
    docker-compose -f $compose pull
    docker-compose -f $compose up -d
done

docker image prune -f
